package pl.riskyocelot.katrina.dao.Impl;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import pl.riskyocelot.katrina.dao.RamTypeDAO;
import pl.riskyocelot.katrina.model.RamType;

import java.util.List;

/**
 * Created by Sdfsgshg on 2016-02-04.
 */
public class RamTypeDAOImpl extends HibernateDaoSupport implements RamTypeDAO {
    public RamTypeDAOImpl(SessionFactory sessionFactory){
        setSessionFactory(sessionFactory);
    }

    public List<RamType> list() {
        return (List<RamType>)getHibernateTemplate().find("from RamType");
    }

    public void save(RamType ramType) {
        getHibernateTemplate().save(ramType);
    }

    public void update(RamType ramType) {
        getHibernateTemplate().update(ramType);
    }

    public void delete(RamType ramType) {
        getHibernateTemplate().delete(ramType);
    }
}
