package pl.riskyocelot.katrina.dao;

import pl.riskyocelot.katrina.model.RamType;

import java.util.List;

/**
 * Created by Sdfsgshg on 2016-02-04.
 */
public interface RamTypeDAO {
    List<RamType> list();
    void save(RamType ramType);
    void update(RamType ramType);
    void delete(RamType ramType);
}
