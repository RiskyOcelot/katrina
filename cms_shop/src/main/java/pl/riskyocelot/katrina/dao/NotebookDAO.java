package pl.riskyocelot.katrina.dao;

import org.springframework.stereotype.Component;
import pl.riskyocelot.katrina.model.Notebook;

import java.util.List;

/**
 * Created by Sdfsgshg on 2016-01-01.
 */
public interface NotebookDAO {
    List<Notebook> list();
    void save(Notebook notebook);
    void update(Notebook notebook);
    void delete(Notebook notebook);
}
