package pl.riskyocelot.katrina.dao;

import pl.riskyocelot.katrina.model.Processor;

import java.util.List;

/**
 * Created by Sdfsgshg on 2016-02-04.
 */
public interface ProcessorDAO {
    List<Processor> list();
    void save(Processor processor);
    void update(Processor processor);
    void delete(Processor processor);
}
