package pl.riskyocelot.katrina.dao.Impl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.riskyocelot.katrina.dao.NotebookDAO;
import pl.riskyocelot.katrina.model.Notebook;

import java.util.List;

/**
 * Created by Sdfsgshg on 2016-01-01.
 */
public class NotebookDAOImpl extends HibernateDaoSupport implements NotebookDAO {

    public NotebookDAOImpl(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

    //@Transactional
    public List<Notebook> list() {
/*        @SuppressWarnings("unchecked")
                List<Notebook> listNotebooks = (List<Notebook>)sessionFactory.getCurrentSession().createCriteria(Notebook.class).setResultTransformer(
                Criteria.DISTINCT_ROOT_ENTITY).list();*/
        return (List<Notebook>)getHibernateTemplate().find("from Notebook");
    }

    public void save(Notebook notebook) {
        getHibernateTemplate().save(notebook);

/*        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(notebook);
        session.getTransaction().commit();
        session.close();*/
    }

    public void update(Notebook notebook) {
        getHibernateTemplate().update(notebook);

/*        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.update(notebook);
        session.getTransaction().commit();
        session.close();*/
    }

    public void delete(Notebook notebook) {
        getHibernateTemplate().delete(notebook);

/*        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(notebook);
        session.getTransaction().commit();
        session.close();*/
    }
}
