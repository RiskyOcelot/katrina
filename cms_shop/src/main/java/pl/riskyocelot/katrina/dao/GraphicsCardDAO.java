package pl.riskyocelot.katrina.dao;

import pl.riskyocelot.katrina.model.GraphicsCard;

import java.util.List;

/**
 * Created by Sdfsgshg on 2016-02-04.
 */
public interface GraphicsCardDAO {
    List<GraphicsCard> list();
    void save(GraphicsCard graphicsCard);
    void update(GraphicsCard graphicsCard);
    void delete(GraphicsCard graphicsCard);
}
