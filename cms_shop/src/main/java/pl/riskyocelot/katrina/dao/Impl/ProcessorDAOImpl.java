package pl.riskyocelot.katrina.dao.Impl;

import org.hibernate.FlushMode;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;
import pl.riskyocelot.katrina.dao.ProcessorDAO;
import pl.riskyocelot.katrina.model.Processor;

import java.util.List;

/**
 * Created by Sdfsgshg on 2016-02-04.
 */
public class ProcessorDAOImpl extends HibernateDaoSupport implements ProcessorDAO {

    public ProcessorDAOImpl(SessionFactory sessionFactory){
        setSessionFactory(sessionFactory);
    }

    public List<Processor> list() {
        return (List<Processor>)getHibernateTemplate().find("from Processor");
    }

    @Transactional
    public void save(Processor processor) {
        getHibernateTemplate().getSessionFactory().getCurrentSession().setFlushMode(FlushMode.AUTO);
        getHibernateTemplate().save(processor);
    }

    public void update(Processor processor) {
        getHibernateTemplate().update(processor);
    }

    public void delete(Processor processor) {
        getHibernateTemplate().delete(processor);
    }
}
