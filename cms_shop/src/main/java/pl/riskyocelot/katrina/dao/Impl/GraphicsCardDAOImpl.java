package pl.riskyocelot.katrina.dao.Impl;

import org.hibernate.FlushMode;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;
import pl.riskyocelot.katrina.dao.GraphicsCardDAO;
import pl.riskyocelot.katrina.model.GraphicsCard;

import java.util.List;

/**
 * Created by Sdfsgshg on 2016-02-04.
 */
public class GraphicsCardDAOImpl extends HibernateDaoSupport implements GraphicsCardDAO {

    public GraphicsCardDAOImpl(SessionFactory sessionFactory){
        setSessionFactory(sessionFactory);
    }

    public List<GraphicsCard> list() {
        return (List<GraphicsCard>)getHibernateTemplate().find("from GraphicsCard");
    }

    @Transactional
    public void save(GraphicsCard graphicsCard) {
        getHibernateTemplate().getSessionFactory().getCurrentSession().setFlushMode(FlushMode.AUTO);
        getHibernateTemplate().save(graphicsCard);
    }

    public void update(GraphicsCard graphicsCard) {
        getHibernateTemplate().update(graphicsCard);
    }

    public void delete(GraphicsCard graphicsCard) {
        getHibernateTemplate().delete(graphicsCard);
    }
}
