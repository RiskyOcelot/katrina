package pl.riskyocelot.katrina.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.riskyocelot.katrina.model.GraphicsCard;
import pl.riskyocelot.katrina.model.Notebook;
import pl.riskyocelot.katrina.model.Processor;
import pl.riskyocelot.katrina.service.GraphicsCardService;
import pl.riskyocelot.katrina.service.NotebookService;
import pl.riskyocelot.katrina.service.ProcessorService;
import pl.riskyocelot.katrina.service.RamTypeService;

import java.util.List;

/**
 * Created by Sdfsgshg on 2016-02-06.
 */
@Controller
@RequestMapping("/edit")
public class NotebookEditorController {

    private static final Logger LOGGER = Logger.getLogger(NotebookEditorController.class);

    private final String INDEX = "edit/index";

    @Autowired
    private NotebookService notebookService;
    @Autowired
    private ProcessorService processorService;
    @Autowired
    private GraphicsCardService graphicsCardService;
    @Autowired
    private RamTypeService ramTypeService;

    @RequestMapping
    public String index()
    {
        return INDEX;
    }

    public void addNotebook()
    {
        Notebook notebook = new Notebook();

        notebookService.save(notebook);
    }

    public List<String> getProcessors() {
        LOGGER.info("Pobieranie nazw CPU");
        return processorService.getProcessorsName();
    }

    public List<String> getGraphicsCards() {
        LOGGER.info("Pobieranie nazw GPU");
        return graphicsCardService.getGraphicsCardsName();
    }

    public List<String> getRamTypes() {
        LOGGER.info("Pobieranie typow RAM");
        return ramTypeService.getRamTypes();
    }
}
