package pl.riskyocelot.katrina.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Sdfsgshg on 2016-01-27.
 */
@Controller
@RequestMapping("/shop")
public class MainController {

    private final String INDEX = "shop/index";

    @RequestMapping
    public String index()
    {
        return INDEX;
    }
}
