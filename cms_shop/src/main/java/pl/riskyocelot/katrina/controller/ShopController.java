package pl.riskyocelot.katrina.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.riskyocelot.katrina.dao.NotebookDAO;
import pl.riskyocelot.katrina.model.Notebook;
import pl.riskyocelot.katrina.service.NotebookService;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by RiskyOcelot on 2015-12-31.
 * Główny kontroler tymczasowy
 */
@Controller
@RequestMapping(value = "/")
public class ShopController {

    private static final Logger LOGGER = Logger.getLogger(ShopController.class);

    private final String INDEX = "index";

    @Autowired
    private NotebookService notebookService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(ModelMap model)
    {
        return INDEX;
    }

    public List<Notebook> getNotebooks()
    {
        LOGGER.info("Notebooki");
        return notebookService.list();
    }
}
