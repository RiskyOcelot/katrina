package pl.riskyocelot.katrina.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import pl.riskyocelot.katrina.dao.NotebookDAO;
import pl.riskyocelot.katrina.model.Notebook;
import pl.riskyocelot.katrina.service.NotebookService;

import java.util.List;

/**
 * Created by Sdfsgshg on 2016-02-04.
 */
@Service
public class NotebookServiceImpl implements NotebookService {

    @Autowired
    private NotebookDAO notebookDAO;

    public List<Notebook> list() {
        return notebookDAO.list();
    }

    public void save(Notebook notebook) {
        notebookDAO.save(notebook);
    }

    public void update(Notebook notebook) {
        notebookDAO.update(notebook);
    }

    public void delete(Notebook notebook) {
        notebookDAO.delete(notebook);
    }
}
