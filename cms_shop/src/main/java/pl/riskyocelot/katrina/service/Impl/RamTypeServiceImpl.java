package pl.riskyocelot.katrina.service.Impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.riskyocelot.katrina.dao.RamTypeDAO;
import pl.riskyocelot.katrina.model.RamType;
import pl.riskyocelot.katrina.service.RamTypeService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sdfsgshg on 2016-02-21.
 */
@Service
public class RamTypeServiceImpl implements RamTypeService {

    @Autowired
    private RamTypeDAO ramTypeDAO;

    public List<RamType> list() {
        return ramTypeDAO.list();
    }

    public void save(RamType ramType) {
        ramTypeDAO.save(ramType);
    }

    public void update(RamType ramType) {
        ramTypeDAO.update(ramType);
    }

    public void delete(RamType ramType) {
        ramTypeDAO.delete(ramType);
    }

    public List<String> getRamTypes()
    {
        List<String> ramTypes = new ArrayList<String>();

        for(RamType ramType : ramTypeDAO.list())
        {
            ramTypes.add(ramType.getRamType());
        }
        return ramTypes;
    }
}
