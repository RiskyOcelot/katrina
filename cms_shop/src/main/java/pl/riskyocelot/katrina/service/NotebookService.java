package pl.riskyocelot.katrina.service;

import pl.riskyocelot.katrina.model.Notebook;

import java.util.List;

/**
 * Created by Sdfsgshg on 2016-02-04.
 */
public interface NotebookService {
    List<Notebook> list();
    void save(Notebook notebook);
    void update(Notebook notebook);
    void delete(Notebook notebook);
}
