package pl.riskyocelot.katrina.service;

import pl.riskyocelot.katrina.model.GraphicsCard;

import java.util.List;

/**
 * Created by Sdfsgshg on 2016-02-07.
 */
public interface GraphicsCardService {
    List<GraphicsCard> list();
    void save(GraphicsCard graphicsCard);
    void update(GraphicsCard graphicsCard);
    void delete(GraphicsCard graphicsCard);
    List<String> getGraphicsCardsName();
}
