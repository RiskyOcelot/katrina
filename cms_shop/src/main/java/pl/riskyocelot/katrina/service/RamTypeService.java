package pl.riskyocelot.katrina.service;

import pl.riskyocelot.katrina.model.RamType;

import java.util.List;

/**
 * Created by Sdfsgshg on 2016-02-21.
 */
public interface RamTypeService {
    List<RamType> list();
    void save(RamType ramType);
    void update(RamType ramType);
    void delete(RamType ramType);
    List<String> getRamTypes();
}
