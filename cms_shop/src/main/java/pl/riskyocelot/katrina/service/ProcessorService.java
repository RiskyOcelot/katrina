package pl.riskyocelot.katrina.service;

import pl.riskyocelot.katrina.model.Processor;

import java.util.List;

/**
 * Created by Sdfsgshg on 2016-02-06.
 */
public interface ProcessorService {
    List<Processor> list();
    void save(Processor processor);
    void update(Processor processor);
    void delete(Processor processor);
    List<String> getProcessorsName();
}
