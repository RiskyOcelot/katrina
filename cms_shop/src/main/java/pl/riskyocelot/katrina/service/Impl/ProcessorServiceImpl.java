package pl.riskyocelot.katrina.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.riskyocelot.katrina.dao.ProcessorDAO;
import pl.riskyocelot.katrina.model.Processor;
import pl.riskyocelot.katrina.service.ProcessorService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sdfsgshg on 2016-02-06.
 */
@Service
public class ProcessorServiceImpl implements ProcessorService {

    @Autowired
    private ProcessorDAO processorDAO;

    public List<Processor> list() {
        return processorDAO.list();
    }

    public void save(Processor processor) {
        processorDAO.save(processor);
    }

    public void update(Processor processor) {
        processorDAO.update(processor);
    }

    public void delete(Processor processor) {
        processorDAO.delete(processor);
    }

    public List<String> getProcessorsName()
    {
        List<String> names = new ArrayList<String>();
        for(Processor processor : processorDAO.list())
        {
            names.add(String.format("%s %s %s", processor.getManufacturer(), processor.getSeries(), processor.getModel()));
        }
        return names;
    }
}
