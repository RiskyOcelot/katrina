package pl.riskyocelot.katrina.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.riskyocelot.katrina.dao.GraphicsCardDAO;
import pl.riskyocelot.katrina.model.GraphicsCard;
import pl.riskyocelot.katrina.service.GraphicsCardService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sdfsgshg on 2016-02-07.
 */
@Service
public class GraphicsCardServiceImpl implements GraphicsCardService {

    @Autowired
    private GraphicsCardDAO graphicsCardDAO;

    public List<GraphicsCard> list() {
        return graphicsCardDAO.list();
    }

    public void save(GraphicsCard graphicsCard) {
        graphicsCardDAO.save(graphicsCard);
    }

    public void update(GraphicsCard graphicsCard) {
        graphicsCardDAO.update(graphicsCard);
    }

    public void delete(GraphicsCard graphicsCard) {
        graphicsCardDAO.delete(graphicsCard);
    }

    public List<String> getGraphicsCardsName()
    {
        List<String> names = new ArrayList<String>();
        for(GraphicsCard card : graphicsCardDAO.list()) {
            names.add(String.format("%s %s %s", card.getManufacturer(), card.getSeries(), card.getModel()).replace("null", ""));
        }
        return names;
    }
}
