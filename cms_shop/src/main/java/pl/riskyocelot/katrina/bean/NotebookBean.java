package pl.riskyocelot.katrina.bean;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import pl.riskyocelot.katrina.model.GraphicsCard;
import pl.riskyocelot.katrina.model.Notebook;
import pl.riskyocelot.katrina.model.Processor;
import pl.riskyocelot.katrina.model.RamType;
import pl.riskyocelot.katrina.service.GraphicsCardService;
import pl.riskyocelot.katrina.service.ProcessorService;

import javax.annotation.ManagedBean;

/**
 * Created by Sdfsgshg on 2016-02-21.
 */
@ManagedBean
public class NotebookBean {

    private final static Logger LOGGER = Logger.getLogger(NotebookBean.class);

    @Autowired
    private ProcessorService processorService;
    @Autowired
    private GraphicsCardService graphicsCardService;

    private Processor processor;
    private RamType ramType;
    private GraphicsCard graphicsCard;

    public NotebookBean()
    {
        processor = new Processor();
        graphicsCard = new GraphicsCard();
    }

    public Processor getProcessor() {
        return processor;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
    }

    public RamType getRamType() {
        return ramType;
    }

    public void setRamType(RamType ramType) {
        this.ramType = ramType;
    }

    public GraphicsCard getGraphicsCard() {
        return graphicsCard;
    }

    public void setGraphicsCard(GraphicsCard graphicsCard) {
        this.graphicsCard = graphicsCard;
    }

    public void addProcessor() {
        if(processor.getParams().isEmpty())
            processor.setParams(null);
        processorService.save(processor);
        LOGGER.info("Zapisano nowy procesor");
    }

    public void addGraphicsCard() {
        graphicsCardService.save(graphicsCard);
        LOGGER.info("Zapisano nowa karta graficzna");
    }
}
