package pl.riskyocelot.katrina.model;

import javax.persistence.*;

/**
 * Created by Sdfsgshg on 2016-02-01.
 */
@Entity
@Table(name = "GRAPHICS_CARDS")
public class GraphicsCard {

    @Id
    @GeneratedValue
    @Column(name = "GRAPHICS_CARD_ID")
    private Long id;

    @Column(name = "MANUFACTURER")
    private String manufacturer;

    @Column(name = "SERIES")
    private String series;

    @Column(name = "MODEL")
    private String model;

    @Column(name = "VRAM_TYPE")
    private String vramType;

    @Column(name = "VRAM_SIZE")
    private Integer vramSize;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVramType() {
        return vramType;
    }

    public void setVramType(String vramType) {
        this.vramType = vramType;
    }

    public Integer getVramSize() {
        return vramSize;
    }

    public void setVramSize(Integer vramSize) {
        this.vramSize = vramSize;
    }
}
