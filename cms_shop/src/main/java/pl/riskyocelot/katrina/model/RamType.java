package pl.riskyocelot.katrina.model;

import javax.persistence.*;

/**
 * Created by Sdfsgshg on 2016-02-01.
 */
@Entity
@Table(name = "ram_types")
public class RamType {

    @Id
    @GeneratedValue
    @Column(name = "RAM_TYPE_ID")
    private Long id;

    @Column(name = "RAM_TYPE")
    private String ramType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRamType() {
        return ramType;
    }

    public void setRamType(String ramType) {
        this.ramType = ramType;
    }
}
