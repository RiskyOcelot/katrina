package pl.riskyocelot.katrina.model;

import com.sun.istack.internal.Nullable;

import javax.persistence.*;

/**
 * Created by Sdfsgshg on 2016-02-01.
 */
@Entity
@Table(name = "processors")
public class Processor {

    @Id
    @GeneratedValue
    @Column(name = "PROCESSOR_ID")
    private Long id;

    @Column(name = "MANUFACTURER")
    private String manufacturer;

    @Column(name = "SERIES")
    private String series;

    @Column(name = "MODEL")
    private String model;

    @Column(name = "CLOCK_SPEED_DOWN")
    private Integer clockSpeedDown;

    @Column(name = "CLOCK_SPEED_UP")
    private Integer clockSpeedUp;

    @Column(name = "CORES")
    private Integer cores;

    @Column(name = "THREADS")
    private Integer threads;

    @Column(name = "CACHE_L1")
    @Nullable
    private Integer cacheL1;

    @Column(name = "CACHE_L2")
    @Nullable
    private Integer cacheL2;

    @Column(name = "CACHE_L3")
    @Nullable
    private Integer cacheL3;

    @Column(name = "PARAMS")
    @Nullable
    private String params;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getClockSpeedDown() {
        return clockSpeedDown;
    }

    public void setClockSpeedDown(Integer clockSpeedDown) {
        this.clockSpeedDown = clockSpeedDown;
    }

    public Integer getClockSpeedUp() {
        return clockSpeedUp;
    }

    public void setClockSpeedUp(Integer clockSpeedUp) {
        this.clockSpeedUp = clockSpeedUp;
    }

    public Integer getCores() {
        return cores;
    }

    public void setCores(Integer cores) {
        this.cores = cores;
    }

    public Integer getThreads() {
        return threads;
    }

    public void setThreads(Integer threads) {
        this.threads = threads;
    }

    public Integer getCacheL1() {
        return cacheL1;
    }

    public void setCacheL1(Integer cacheL1) {
        this.cacheL1 = cacheL1;
    }

    public Integer getCacheL2() {
        return cacheL2;
    }

    public void setCacheL2(Integer cacheL2) {
        this.cacheL2 = cacheL2;
    }

    public Integer getCacheL3() {
        return cacheL3;
    }

    public void setCacheL3(Integer cacheL3) {
        this.cacheL3 = cacheL3;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }
}
