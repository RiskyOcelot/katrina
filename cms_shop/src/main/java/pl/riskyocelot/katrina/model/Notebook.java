package pl.riskyocelot.katrina.model;

import com.sun.istack.internal.Nullable;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * Created by Sdfsgshg on 2015-12-31.
 */
@Entity
@Table(name = "notebooks")
public class Notebook {
    @Id
    @GeneratedValue
    @Column(name = "NOTEBOOK_ID")
    private long id;
    @Column(name = "MANUFACTURER")
    private String manufacturer;
    @Column(name = "SERIES")
    @Nullable
    private String series;
    @Column(name = "MODEL")
    private String model;
    @Column(name = "DIAGONAL")
    private String diagonal;
    @ManyToOne
    @JoinColumn(name = "PROCESSOR")
    private Processor processor;
    @ManyToOne
    @JoinColumn(name = "RAM_TYPE")
    private RamType ramType;
    @Nullable
    @Column(name = "RAM_SIZE")
    private String ramSize;
    @ManyToOne
    @JoinColumn(name = "GRAPHICS_CARD")
    private GraphicsCard graphicsCard;
    @Column(name = "VALUE")
    private BigDecimal value;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufactorer)
    {
        this.manufacturer = manufacturer;
    }

    public String getDiagonal() {
        return diagonal;
    }

    public void setDiagonal(String diagonal) {
        this.diagonal = diagonal;
    }

    public GraphicsCard getGraphicsCard() {
        return graphicsCard;
    }

    public void setGraphicsCard(GraphicsCard graphicsCard) {
        this.graphicsCard = graphicsCard;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Processor getProcessor() {
        return processor;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
    }

    public String getRamSize() {
        return ramSize;
    }

    public void setRamSize(String ramSize) {
        this.ramSize = ramSize;
    }

    public RamType getRamType() {
        return ramType;
    }

    public void setRamType(RamType ramType) {
        this.ramType = ramType;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
